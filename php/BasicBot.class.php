<?php

define('MAX_LINE_LENGTH', 1024 * 1024);

class BasicBot {

    protected $sock, $debug;
    private $my_car_color; // color of my car
	private $turbo_available = false; // when turbo is available
    private $pieces_array = array(); // array with information for each piece
	private $throttle_straigh = 1;
	private $throttle_open_corner = 0.7;
	private $throttle_normal_corner = 0.5;
	private $throttle_closed_corner = 0.2;

    function __construct($host, $port, $botname, $botkey, $debug = FALSE) {
        $this->debug = $debug;
        $this->connect($host, $port, $botkey);
		
		$this->write_msg('join', array(
            'name' => $botname,
            'key' => $botkey
        ));
		
		
		/*
		// create race for new track
		$this->write_msg('joinRace', array(
			'botId' => array(
				'name' => $botname,
				'key' => $botkey
			),
			'trackName' => 'germany', // keimola, germany, usa, france
			'carCount' => 1)
		);
		*/
    }

    function __destruct() {
        if (isset($this->sock)) {
            socket_close($this->sock);
        }
    }

    protected function connect($host, $port, $botkey) {
        $this->sock = @ socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        if ($this->sock === FALSE) {
            throw new Exception('socket: ' . socket_strerror(socket_last_error()));
        }
        if (@!socket_connect($this->sock, $host, $port)) {
            throw new Exception($host . ': ' . $this->sockerror());
        }
    }

    protected function read_msg() {
        $line = @ socket_read($this->sock, MAX_LINE_LENGTH, PHP_NORMAL_READ);
        if ($line === FALSE) {
            $this->debug('** ' . $this->sockerror());
        } else {
            $this->debug('<= ' . rtrim($line));
        }
        return json_decode($line, TRUE);
    }

    protected function write_msg($msgtype, $data) {
        $str = json_encode(array('msgType' => $msgtype, 'data' => $data)) . "\n";
        $this->debug('=> ' . rtrim($str));
        if (@ socket_write($this->sock, $str) === FALSE) {
            throw new Exception('write: ' . $this->sockerror());
        }
    }

    protected function sockerror() {
        return socket_strerror(socket_last_error($this->sock));
    }

    protected function debug($msg) {
        if ($this->debug) {
            echo $msg, "\n";
        }
    }

    public function run() {
		$currentIndex = 0;
		$previousIndex = 0;
        while (!is_null($msg = $this->read_msg())) {
            switch ($msg['msgType']) {
				case 'turboAvailable':
					// if turbo is available
					$this->turbo_available = true;
					break;
                case 'carPositions':
					// find the id of my car
					$cars = $msg['data'];
					$my_car_id = $this->my_car_index($cars);
					
					// user turbo if piece is set to 5 and turbo is available
					// (this has to be first beacuse we can send only one message at a time in one tick)
					$this->use_turbo_of_piece($currentIndex);
					
                    // use throttle for each piece according to piece_array initialized from game init
					$pieceIndex = $msg['data'][$my_car_id]['piecePosition']['pieceIndex'];
					$this->use_throttle_of_piece($currentIndex);
					
					// count each piece to get current piece index
					if($previousIndex != $pieceIndex) {
						$currentIndex++;
					}
					$previousIndex = $pieceIndex;
					
					$this->write_msg('ping', null);
                    break;
                case 'join':
                    break;
                case 'yourCar':
					// find color of my car
                    $this->color_of_my_car($msg['data']['color']);
                    break;
                case 'gameInit':
					// set pieces to pieces_array
					$pieces = $msg['data']['race']['track']['pieces'];
					
					// if Qualifying or Race
					if(isset($msg['data']['race']['raceSession']['laps'])) {
						$laps = $msg['data']['race']['raceSession']['laps']; // on Race
					} else {
						$laps = 50; // on Qualifying
 					}
					
					$this->set_pieces($pieces, $laps);
					
					// slow down if the car is before a corner
					$this->set_throttle_to_slow_down_before_normal_corner();
					
					// slow down if the car is before a corner
					$this->set_throttle_to_slow_down_before_closed_corner();
					
					// set piece value to "5" when to use turbo
					// (this has to be last so that tha change of piece to 5
					// will not change the previous functions about the throttle of the corners)
					$this->set_piece_when_to_use_turbo();
					
					// set turbo before the end of the race
					$this->set_turbo_to_the_last_pieces();
                    break;
                case 'gameStart':
                    break;
                case 'crash':
                    break;
                case 'spawn':
                    break;
                case 'lapFinished':
                    break;
                case 'dnf':
                    break;
                case 'finish':
                    break;
                default:
                    $this->write_msg('ping', null);
            }
        }
    }

    private function color_of_my_car($color) {
		/* what is the color of my car */
        $this->my_car_color = $color;
    }

	private function my_car_index($cars) {
		/* after knowing the color of my car
		 * what is the id of my car
		 */
		for ($c = 0; $c < count($cars); $c++) {
            if ($cars[$c]['id']['color'] == $this->my_car_color) {
                return $c;
            }
        }
    }
	
	private function set_pieces($pieces, $laps) {
		/* if piece=>corner is straight set to "1"
		 * if piece=>corner is an open corner set to "2"
		 * if piece=>corner is a normal corner set to "3"
         * if piece=>corner is a closed corner set to "4"
		 * if 5 pieces in a row are straight set to "5" from function "set_piece_when_to_use_turbo()"
         */
		$all_pieces = count($pieces);
        for ($p = 0; $p < count($pieces) * $laps; $p++) {
			$current_lap = floor($p / $all_pieces);
            if (isset($pieces[($p - ($all_pieces * $current_lap))]['length'])) {
                $this->pieces_array[] = array(
					'corner' => 1,
					'throttle' => $this->throttle_straigh
				);
            } else {
				if ($pieces[($p - ($all_pieces * $current_lap))]['radius'] > 100) {
					$this->pieces_array[] = array(
						'corner' => 2,
						'throttle' => $this->throttle_open_corner
					);
				} else if ($pieces[($p - ($all_pieces * $current_lap))]['radius'] == 100) {
					$this->pieces_array[] = array(
						'corner' => 3,
						'throttle' => $this->throttle_normal_corner
					);
				} else if ($pieces[($p - ($all_pieces * $current_lap))]['radius'] < 100) {
					$this->pieces_array[] = array(
						'corner' => 4,
						'throttle' => $this->throttle_closed_corner
					);
				}
            }
        }
	}
	
	private function set_throttle_to_slow_down_before_normal_corner() {
		$straight_pieces = 0;
		for ($p = 1; $p < count($this->pieces_array); $p++) {
            if ($this->pieces_array[$p]['corner'] == 3) {
				if (($this->pieces_array[$p - 1]['corner'] == 1) || ($this->pieces_array[$p - 1]['corner'] == 2)) {
					// calculate distance (pieces back) tha car has to slow down
					$pieces_back = round($straight_pieces * 0.6);
					for ($b = 1; $b <= $pieces_back; $b++) {
						$this->pieces_array[$p - $b]['throttle'] = $this->throttle_closed_corner;
					}
					$straight_pieces = 0;
				}
            } else {
				if (($this->pieces_array[$p - 1]['corner'] == 1) || ($this->pieces_array[$p - 1]['corner'] == 2)) {
					$straight_pieces++;
				} else {
					$straight_pieces = 0;
				}	
            }
        }
	}
	
	private function set_throttle_to_slow_down_before_closed_corner() {
		$straight_pieces = 0;
		for ($p = 1; $p < count($this->pieces_array); $p++) {
            if ($this->pieces_array[$p]['corner'] == 4) {
				if (($this->pieces_array[$p - 1]['corner'] == 1) || ($this->pieces_array[$p - 1]['corner'] == 2) || ($this->pieces_array[$p - 1]['corner'] == 3)) {
					// calculate distance (pieces back) tha car has to slow down
					$pieces_back = round($straight_pieces * 0.6);
					for ($b = 1; $b <= $pieces_back; $b++) {
						$this->pieces_array[$p - $b]['throttle'] = $this->throttle_closed_corner;
					}
					$straight_pieces = 0;
				}
            } else {
				if (($this->pieces_array[$p - 1]['corner'] == 1) || ($this->pieces_array[$p - 1]['corner'] == 2) || ($this->pieces_array[$p - 1]['corner'] == 3)) {
					$straight_pieces++;
				} else {
					$straight_pieces = 0;
				}	
            }
        }
	}
	
	private function set_piece_when_to_use_turbo() {
		$straight_pieces = 0;
		for ($p = 0; $p < count($this->pieces_array); $p++) {
            if ($this->pieces_array[$p]['corner'] == 1) {
				$straight_pieces++;
			} else {
				$straight_pieces = 0;
			}
			
			if ($straight_pieces == 6) {
				$this->pieces_array[$p - 6]['corner'] = 5;
			}
		}
	}
	
	private function set_turbo_to_the_last_pieces() {
		$last_pieces = count($this->pieces_array) - 5;
		$this->pieces_array[$last_pieces]['corner'] = 5;
	}
	
	private function use_turbo_of_piece($currentIndex) {
		if(($this->pieces_array[$currentIndex]['corner'] == 5) && ($this->turbo_available == true)) {
			$this->write_msg('turbo', 'Go go power ranger...');
			$this->turbo_available = false;
		}
	}
	
	private function use_throttle_of_piece($currentIndex) {
		$this->write_msg('throttle', $this->pieces_array[$currentIndex]['throttle']);	
	}

}

?>